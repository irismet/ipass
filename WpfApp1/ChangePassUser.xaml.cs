﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.Data;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class ChangePassUser : Window
    {
        SQLiteConnection db_con = new SQLiteConnection();
        string old_pass;
        string login;
        string contr;
        string uid;
        string today;
        
        public ChangePassUser()
        {
            InitializeComponent();
            try
            {
                today = DateTime.Now.ToShortDateString() + " : " + DateTime.Now.ToLongTimeString();
                db_con.ConnectionString = "Data Source = .\\family.db3";
                db_con.Open();
                /*SQLiteCommand cmd1 = new SQLiteCommand("select type_name from types ", db_con);
                SQLiteDataReader dbr1 = cmd1.ExecuteReader();
                while (dbr1.Read())
                {
                    CBContr.Items.Add(dbr1.GetString(0));
                    CBContr.SelectedIndex = 0;
                }*/
                SQLiteCommand cmd2 = new SQLiteCommand("select cast(id as text), login, contract from login where login = '" + AuthorizedWindow.strLogin + "'", db_con);
                SQLiteDataReader dbr2 = cmd2.ExecuteReader();
                while (dbr2.Read())
                {
                    uid = dbr2.GetString(0);
                    CBLogin.Items.Add(dbr2.GetString(1));
                    CBLogin.SelectedIndex = 0;
                    contr = dbr2.GetString(2).ToString();
                }

                SQLiteCommand cmd1 = new SQLiteCommand("select type_name from types where id = " + contr, db_con);
                SQLiteDataReader dbr1 = cmd1.ExecuteReader();
                while (dbr1.Read())
                {
                    CBContr.Items.Add(dbr1.GetString(0));
                    CBContr.SelectedIndex = 0;
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                db_con.Close();
            }
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (db_con.State != ConnectionState.Open)
                {
                    db_con.Open();
                }

                if (Old_pass.Password.Length != 0 && New_pass.Password.Length != 0 && Confirm_pass.Password.Length != 0)
                {
                    if (New_pass.Password == Confirm_pass.Password)
                    {
                        SQLiteCommand cmd1 = new SQLiteCommand("select user_pass, user_name, cast(pass_type as text) from users where id = " + uid, db_con);
                        SQLiteDataReader dbr1 = cmd1.ExecuteReader();
                        while (dbr1.Read())
                        {
                            old_pass = dbr1.GetString(0);
                            login    = dbr1.GetString(1);
                            contr    = dbr1.GetString(2);
                        }
                            if (CBLogin.SelectedValue.ToString() == login && (CBContr.SelectedIndex + 1).ToString() == contr && Old_pass.Password == old_pass)
                            {
                                try
                                {
                                    SQLiteCommand cmd2 = new SQLiteCommand("update users set user_pass = '" + New_pass.Password + "' where id = " + uid, db_con);
                                    SQLiteDataReader dbr2 = cmd2.ExecuteReader();
                                    MessageBox.Show("Пароль успешно изменен");

                                try
                                {
                                    SQLiteCommand wrt_hist = new SQLiteCommand
                                            ("insert into history('modif_date', 'modif_type', 'user', 'login', 'details') " +
                                            "values (" + "'" + today + "', 'Change password', '', " + "'" + login + "', " +
                                            "'Операция выполнена успешно пароль сменен с "
                                            + old_pass + " на " + New_pass.Password +
                                            " с назначением " + CBContr.SelectedValue.ToString() + "')", db_con);
                                    wrt_hist.ExecuteNonQueryAsync();
                                }
                                catch (SQLiteException ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                                MainWindow mnw = new MainWindow();
                                this.Close();
                                mnw.ShowDialog();
                                }
                                catch (SQLiteException ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            finally
                            {
                                if (db_con.State != ConnectionState.Closed)
                                {
                                    db_con.ClearCachedSettings();
                                    db_con.Close();
                                }
                            }
                            }
                            else
                            {
                            try
                            {
                                if (db_con.State != ConnectionState.Open)
                                { 
                                    db_con.Open();
                                }
                                SQLiteCommand wrt_hist = new SQLiteCommand
                                        ("insert into history('modif_date', 'modif_type', 'user', 'login', 'details') " +
                                        "values (" + "'" + today + "', 'Change password', '', " + "'" + login + "', " +
                                        "'Операция не выполнена, пароль пытались сменить с "
                                        + Old_pass.Password + " на " + New_pass.Password +
                                        " с назначением " + CBContr.SelectedValue.ToString() + "')", db_con);
                                wrt_hist.ExecuteNonQueryAsync();
                            }
                            catch (SQLiteException ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            finally
                            {
                                if (db_con.State != ConnectionState.Closed)
                                {
                                    db_con.ClearCachedSettings();
                                    db_con.Close();
                                }
                            }
                            MessageBox.Show("Введены некорректные данные, логин, назначение или пароль!", "Ошибка");
                            //MessageBox.Show(CBLogin.SelectedValue.ToString() + " " + login + " " + (CBContr.SelectedIndex + 1).ToString() + " " + contr + " " + Old_pass.Password + " <- op form   -> old_pass db" + old_pass);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (db_con.State != ConnectionState.Open)
                            {
                                db_con.Open();
                            }
                            SQLiteCommand wrt_hist = new SQLiteCommand
                                    ("insert into history('modif_date', 'modif_type', 'user', 'login', 'details') " +
                                    "values (" + "'" + today + "', 'Change password', '', " + "'" + login + "', " +
                                    "'Операция не выполнена, пароль пытались сменить с "
                                    + Old_pass.Password + " на " + New_pass.Password +
                                    " с назначением " + CBContr.SelectedValue.ToString() + " но не совпало подтверждение')", db_con);
                            wrt_hist.ExecuteNonQueryAsync();
                        }
                        catch (SQLiteException ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            if (db_con.State != ConnectionState.Closed)
                            {
                                db_con.ClearCachedSettings();
                                db_con.Close();
                            }
                        }
                        MessageBox.Show("Некорректно введено подтверждение нового пароля", "Ошибка");
                }
                }
                else
                {
                    MessageBox.Show("Одно из полей формы не заполнено, повторите ввод","Ошибка");
                }
            }
            catch(SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (db_con.State != ConnectionState.Closed)
                {
                    db_con.ClearCachedSettings();
                    db_con.Close();
                }
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            Old_pass.Clear();
            New_pass.Clear();
            Confirm_pass.Clear();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (db_con.State != ConnectionState.Closed)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
            AuthorizedWindow auth = new AuthorizedWindow();
            this.Close();
            auth.ShowDialog();
        }
    }
}
