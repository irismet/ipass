﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.IO;
using System.Data;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SQLiteConnection db_con = new SQLiteConnection();

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                LblInfo.Content = "Вы вошли под логином " + AuthorizedWindow.strLogin + "! \n" + "Назначение " + AuthorizedWindow.strContr;
                db_con.ConnectionString = "Data Source=.\\family.db3";
                db_con.Open();
                SQLiteCommand cmd2 = new SQLiteCommand("select login_out, user_pass from users where " +
                                                       " users.user_name = '" + AuthorizedWindow.strLogin + "'"
                                                       + " and users.pass_type = '" + AuthorizedWindow.strContr + "'", db_con);
                SQLiteDataReader dbr2 = cmd2.ExecuteReader();
                
                while (dbr2.Read())
                {
                    if (dbr2.GetString(0).Length != 0 && dbr2.GetString(1).Length != 0)
                    {
                        textBox.Text = dbr2.GetString(0).ToString();
                        PassBox.Password = dbr2.GetString(1).ToString();
                    }
                    else
                    {
                        textBox.Text = "Не найдена запись, обратитесь к администратору";
                        PassBox.Password = "";
                    }
                }
            }
            catch(SQLiteException ex)
            {
                MessageBox.Show("Ошибка + " + ex.Message, "Ошибка подключения к БД");
             }
            finally
            {
                db_con.Close();
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(textBox.Text);
            MessageBox.Show("Скопировано!", "Уведомление");
         }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(PassBox.Password);
            MessageBox.Show("Скопировано!", "Уведомление");
        }

        private void BuutonCancel_Click(object sender, RoutedEventArgs e)
        {
            AuthorizedWindow authwin = new AuthorizedWindow();
            AuthorizedWindow.strLogin = "";
            AuthorizedWindow.strContr = "";
            this.Close();
            authwin.ShowDialog();
            if (db_con.State == System.Data.ConnectionState.Open)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
        }

        private void ButtonChangePass_Click(object sender, RoutedEventArgs e)
        {
            ChangePassUser chpl = new ChangePassUser();
            if (db_con.State != ConnectionState.Closed)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
            this.Close();
            chpl.ShowDialog();
        }
    }
}
