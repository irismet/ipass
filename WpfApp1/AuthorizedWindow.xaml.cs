﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Common;
using System.Data.SQLite;
using System.Collections;
using System.Data;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для AuthorizedWindow.xaml
    /// </summary>
    public partial class AuthorizedWindow : Window
    {
        SQLiteConnection db_con = new SQLiteConnection();
        public static string strLogin;
        public static string strContr;
        private string strPass;
        ArrayList lst = new ArrayList();

        public AuthorizedWindow()
        {
            InitializeComponent();
                db_con.ConnectionString = "Data Source=.\\family.db3";
            db_con.Open();
            SQLiteCommand cmd2 = new SQLiteCommand("select type_name from types", db_con);
                SQLiteDataReader dbr2 = cmd2.ExecuteReader();
                while (dbr2.Read())
                {
                  // lst.Add(dbr2.GetString(0));
                    //lst.Add(dbr2.GetString(1));
                    CBContract.Items.Add(dbr2.GetString(0));
                CBContract.SelectedIndex = 0;
                }
            db_con.Close();
         }
        
        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            string today = DateTime.Now.ToShortDateString() + " : " + DateTime.Now.ToLongTimeString();
            try
            {
                if (db_con.State != ConnectionState.Open)
                {
                    db_con.Open();
                }
                    string uname = TextBoxLogin.Text;
                    string upass = PasswordBoxPass.Password;
                    string ucontr = (CBContract.SelectedIndex + 1).ToString();
                if (uname.Length != 0 && upass.Length != 0 && ucontr.Length != 0)
                {
                    SQLiteCommand cmd = new SQLiteCommand("select login, password, contract from login where " +
                                                          "login.login = '" +
                                                          uname + "'" +
                                                          " and login.password = '"
                                                          + upass + "'"
                                                          + " and login.contract = '"
                                                          + ucontr + "'", db_con);

                    SQLiteDataReader dbreader = cmd.ExecuteReader();
                    while (dbreader.Read())
                    {
                        strLogin = dbreader.GetString(0);
                        strPass = dbreader.GetString(1);
                        strContr = dbreader.GetString(2);
                    }
                }
                //MessageBox.Show(strLogin + " " + strPass + " " + strContr);
                if (strLogin == uname && strPass == upass && strContr.ToString() == ucontr)
                    {
                        MainWindow w1 = new MainWindow();
                    try
                    {
                        SQLiteCommand wrt_hist = new SQLiteCommand
                                ("insert into history('modif_date', 'modif_type', 'user', 'login', 'details') " +
                                "values ('" + today + "' , 'Click on button OK', '', " +
                                "'" + TextBoxLogin.Text + "', 'Пользователь авторизовался')", db_con);
                        wrt_hist.ExecuteNonQueryAsync();
                    }
                    catch(SQLiteException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    this.Close();
                    w1.ShowDialog();

                    if (db_con.State == ConnectionState.Open)
                    {
                        db_con.Close();
                    }
                  }

                    else
                    {
                    try
                    {
                        SQLiteCommand wrt_hist = new SQLiteCommand
                                ("insert into history('modif_date', 'modif_type', 'user', 'login', 'details') " +
                                "values (" + "'" + today  + "', 'Click on button OK', '', " + "'" + TextBoxLogin.Text + "', " +
                                "'Не удалось авторизоваться! Введенные данные: Логин " 
                                + TextBoxLogin.Text + " пароль " + PasswordBoxPass.Password + 
                                " назначение " + CBContract.SelectedValue.ToString() + "')", db_con);
                        wrt_hist.ExecuteNonQueryAsync();
                    }
                    catch (SQLiteException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    if (TextBoxLogin.Text.Length == 0)
                        {
                            MessageBox.Show("Вы не заполнили обязательное поле 'Логин'", "Ошибка авторизации");
                        }
                        if (PasswordBoxPass.Password.Length == 0)
                        {
                            MessageBox.Show("Вы не заполнили обязательное поле 'Пароль'", "Ошибка авторизации");
                        }
                        else
                        {
                            MessageBox.Show("Логин, пароль или назначение неверны!", "Ошибка авторизации");
                        }
                    }
                }
          catch(SQLiteException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка подключения к базе данных! Обратитесь к администратору!");
            }
            finally
            {
                db_con.Close();
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            TextBoxLogin.Clear();
            PasswordBoxPass.Clear();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (db_con.State != ConnectionState.Closed)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
            this.Close();
        }

        private void ButtonCreateUser_Click(object sender, RoutedEventArgs e)
        {
            RegistrWindow w2 = new RegistrWindow();
            if (db_con.State != ConnectionState.Closed)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
            this.Close();
            w2.ShowDialog();
        }
        
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void ButtonChangePass_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
