﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.Data;
using System.Collections;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для Window2.xaml
    /// </summary>
    public partial class RegistrWindow : Window
    {
        SQLiteConnection db_con = new SQLiteConnection();
        string strLogin = "";
        string strContr = "";

        public RegistrWindow()
        {
            InitializeComponent();
            ArrayList lst = new ArrayList();
            try
            {
                db_con.ConnectionString = "Data Source=.\\family.db3";
                if (db_con.State != ConnectionState.Open)
                {
                    db_con.Open();
                }
                SQLiteCommand cmd2 = new SQLiteCommand("select type_name from types", db_con);
                SQLiteDataReader dbr2 = cmd2.ExecuteReader();
                while (dbr2.Read())
                {
                    lst.Add(dbr2.GetString(0));
                    //lst.Add(dbr2.GetString(1));
                    CBContract.Items.Add(dbr2.GetString(0));
                    CBContract.SelectedIndex = 0;
                }
            }
            catch(SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
            }

        private void BuutonReset_Click(object sender, RoutedEventArgs e)
        {
            TBLogin.Clear();
            PBPass.Clear();
            TBAppLogin.Clear();
            PBAppPass.Clear();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {

            if (db_con.State != ConnectionState.Closed)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }

            AuthorizedWindow auw = new AuthorizedWindow();
            this.Close();
            auw.ShowDialog();
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            if (TBLogin.Text.Length != 0 && PBPass.Password.Length != 0 && 
               CBContract.Text.Length != 0 && TBAppLogin.Text.Length != 0 && PBAppPass.Password.Length != 0)
            {
                if (db_con.State != ConnectionState.Open)
                {
                    db_con.Open();
                }
                try
                {
                    SQLiteCommand cmd_crt = new SQLiteCommand("insert into login('login', 'password', 'contract') values (" + "'" + TBLogin.Text.ToString() + "', " + "'" + PBPass.Password + "', " + "'" + (CBContract.SelectedIndex + 1).ToString() + "')", db_con);
                    cmd_crt.ExecuteNonQuery();

                    string uname = TBLogin.Text;
                    string ucontr = (CBContract.SelectedIndex + 1).ToString();
                    if (uname.Length != 0 && ucontr.Length != 0)
                    {
                        SQLiteCommand cmd = new SQLiteCommand("select login, contract from login where " +
                                                              "login.login = '" +
                                                              uname + "'" +
                                                              " and login.contract = '"
                                                              + ucontr + "'", db_con);

                        SQLiteDataReader dbreader = cmd.ExecuteReader();
                        while (dbreader.Read())
                        {
                            strLogin = dbreader.GetString(0);
                            strContr = dbreader.GetString(1);
                        }
                    }

                    SQLiteCommand cmd_crt_usr = new SQLiteCommand("insert into users('user_name', 'user_pass', 'pass_type', 'login_out') values (" + "'" + strLogin + "', " + "'" + PBAppPass.Password + "', " + "'" + strContr + "', " + "'" + TBAppLogin.Text.ToString() + "')", db_con);
                    cmd_crt_usr.ExecuteNonQuery();
                  
                    MessageBox.Show("Запись успешно создана", "Success");

                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    AuthorizedWindow auw = new AuthorizedWindow();
                    if (db_con.State != ConnectionState.Open)
                    {
                        db_con.ClearCachedSettings();
                        db_con.Close();
                    }
                    this.Close();
                    auw.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Одно из полей формы не заплнено, проверьте корректность заполнения формы","Ошибка");
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BTAddContract_Click(object sender, RoutedEventArgs e)
        {
            AddContract adc = new AddContract();
         
            if (db_con.State != ConnectionState.Closed)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
            this.Close();
            adc.ShowDialog();
        }
    }
}
