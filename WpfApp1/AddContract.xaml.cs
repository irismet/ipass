﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Data;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для AddContract.xaml
    /// </summary>
    public partial class AddContract : Window
    {
        SQLiteConnection db_con = new SQLiteConnection();

        public AddContract()
        {
            db_con.ConnectionString = "Data Source=.\\family.db3";
            if (db_con.State != ConnectionState.Open)
            {
                db_con.Open();
            }
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (TBAddContract.Text.Length != 0)
            {
              
                try
                {
                    if (db_con.State != ConnectionState.Open)
                    {
                        db_con.Open();
                    }
                    SQLiteCommand cmd_crt = new SQLiteCommand("insert into types('type_name') values (" + "'" + TBAddContract.Text.ToString() + "')", db_con);
                    cmd_crt.ExecuteNonQuery();
                    MessageBox.Show("Запись успешно создана", "Success");
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    RegistrWindow rgw = new RegistrWindow();
                    if (db_con.State == ConnectionState.Open)
                    {
                        db_con.ClearCachedSettings();
                        db_con.Close();
                    }
                    this.Close();
                    rgw.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Одно из полей формы не заплнено, проверьте корректность заполнения формы", "Ошибка");
            }
        }

        private void BTCancel_Click(object sender, RoutedEventArgs e)
        {
            RegistrWindow rgw = new RegistrWindow();
            if (db_con.State == ConnectionState.Open)
            {
                db_con.ClearCachedSettings();
                db_con.Close();
            }
            this.Close();
            rgw.ShowDialog();
        }
    }
    }

